package main

import (
	"log"

	"github.com/kadycui/fiber-ci/database"
	_ "github.com/kadycui/fiber-ci/docs"
	"github.com/kadycui/fiber-ci/models"
	"github.com/kadycui/fiber-ci/routes"
)

// @title Book App
// @version 1.0
// @description 这是一个Book管理程序

// @contact.name kadycui
// @contact.email kadycui@qq.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api
func main() {
	app := routes.New()

	if err := database.Connect(); err != nil {
		log.Panic("Can't connect database:", err.Error())
	}

	database.DBConn.AutoMigrate(&models.User{}, &models.Book{}, &models.Product{})
	log.Fatal(app.Listen(":3000"))
}
