package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/swagger"
	"github.com/gofiber/template/html/v2"
	_ "github.com/kadycui/fiber-ci/docs"
	"github.com/kadycui/fiber-ci/handlers"
	"github.com/kadycui/fiber-ci/middleware"
)

// New create an instance of Book app routes
func New() *fiber.App {

	engine := html.New("./views", ".html")
	app := fiber.New(fiber.Config{
		Views: engine,
	})

	app.Use(cors.New())

	app.Get("/", func(c *fiber.Ctx) error {
		// Render index template
		return c.Render("index", fiber.Map{
			"Title": "Hello, World!",
		})
	})

	app.Get("/swagger/*", swagger.HandlerDefault) // default

	app.Get("/swagger/*", swagger.New(swagger.Config{ // custom
		URL:         "http://example.com/doc.json",
		DeepLinking: false,
		// Expand ("list") or Collapse ("none") tag groups by default
		DocExpansion: "none",
		// Prefill OAuth ClientId on Authorize popup
		OAuth: &swagger.OAuthConfig{
			AppName:  "OAuth Provider",
			ClientId: "21bb4edc-05a7-4afc-86f1-2e151e4ba6e2",
		},
		// Ability to change OAuth2 redirect uri location
		OAuth2RedirectUrl: "http://localhost:8080/swagger/oauth2-redirect.html",
	}))

	// Middleware
	api := app.Group("/api", logger.New(logger.Config{
		// Format:     "${cyan}[${time}] ${white}${pid} ${red}${status} ${blue}[${method}] ${white}${path}\n",
		TimeFormat: "2006-01-02 15:04:05",
		TimeZone:   "Asia/Shanghai",
	}))

	api.Get("/", handlers.Hello)
	// Auth
	api.Post("/auth/login", handlers.Login)

	v1 := app.Group("/api/v1")
	{

		// User
		v1.Get("/user/:id", handlers.GetUser)
		v1.Post("/user", handlers.CreateUser)
		v1.Patch("/user/:id", middleware.Protected(), handlers.UpdateUser)
		v1.Delete("/user/:id", middleware.Protected(), handlers.DeleteUser)

		// Book
		v1.Get("/book", handlers.GetAllBooks)
		v1.Get("/book/:id", handlers.GetBookByID)
		v1.Post("/book", handlers.RegisterBook)
		v1.Delete("/book/:id", handlers.DeleteBook)
		v1.Put("/book/:id", handlers.UpdateBook)

		// Product
		v1.Get("/product", handlers.GetAllProduct)
		v1.Get("product/:id", handlers.GetProductID)
		v1.Post("/product", handlers.RegisterProduct)
		v1.Delete("/product/:id", handlers.DeleteProduct)
		v1.Put("/product/:id", handlers.UpdateProduct)

	}

	return app
}
