package models

import "gorm.io/gorm"

// Book is a model for book
type Book struct {
	ID        uint   `gorm:"primarykey"`
	Title     string `gorm:"not null" json:"title" example:"三重门"`
	Author    string `gorm:"not null" json:"author" example:"韩寒"`
	Publisher string `gorm:"not null" json:"publisher" example:"作家出版社"`
	gorm.Model
}

type BodyBook struct {
	Title     string
	Author    string
	Publisher string
}
