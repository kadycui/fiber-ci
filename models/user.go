package models

import "gorm.io/gorm"

// User struct
type User struct {
	ID       uint   `gorm:"primarykey"`
	Username string `gorm:"uniqueIndex;not null" json:"username"`
	Email    string `gorm:"uniqueIndex;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Names    string `json:"names"`
	gorm.Model
}

type NewUser struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

type LoginInput struct {
	Identity string `json:"identity"`
	Password string `json:"password"`
}
type UserData struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
