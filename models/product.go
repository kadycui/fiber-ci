package models

import "gorm.io/gorm"

type Product struct {
	ID          uint   `gorm:"primarykey"`
	Title       string `gorm:"not null" json:"title" example:"华为P60"`
	Description string `gorm:"not null" json:"description" example:"麒麟9100"`
	Amount      int    `gorm:"not null" json:"amount" example:"6999"`
	gorm.Model
}

type BodyProduct struct {
	Title       string
	Description string
	Amount      int
}
