package handlers

import (
	"fmt"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/kadycui/fiber-ci/database"
	"github.com/kadycui/fiber-ci/models"
)

// ResponseHTTP represents response body of this API
type ResponseHTTP struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

// GetAllBooks is a function to get all books data from database
// @Summary 查询全部图书
// @Description 查询全部图书
// @Tags Book
// @Accept json
// @Produce json
// @Success 200 {object} ResponseHTTP{data=[]models.Book}
// @Failure 503 {object} ResponseHTTP{}
// @Router /v1/book [get]
func GetAllBooks(c *fiber.Ctx) error {
	db := database.DBConn

	var books []models.Book
	if res := db.Find(&books); res.Error != nil {
		return c.Status(http.StatusServiceUnavailable).JSON(ResponseHTTP{
			Success: false,
			Message: res.Error.Error(),
			Data:    nil,
		})
	}

	return c.JSON(ResponseHTTP{
		Success: true,
		Message: "Success get all books.",
		Data:    books,
	})
}

// GetBookByID is a function to get a book by ID
// @Summary 通过ID获取一本图书
// @Description 通过ID获取一本图书
// @Tags Book
// @Accept json
// @Produce json
// @Param id path int true "Book ID"
// @Success 200 {object} ResponseHTTP{data=[]models.Book}
// @Failure 404 {object} ResponseHTTP{}
// @Failure 503 {object} ResponseHTTP{}
// @Router /v1/book/{id} [get]
func GetBookByID(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn

	book := new(models.Book)
	if err := db.First(&book, id).Error; err != nil {
		switch err.Error() {
		case "record not found":
			return c.Status(http.StatusNotFound).JSON(ResponseHTTP{
				Success: false,
				Message: fmt.Sprintf("Book with ID %v not found.", id),
				Data:    nil,
			})
		default:
			return c.Status(http.StatusServiceUnavailable).JSON(ResponseHTTP{
				Success: false,
				Message: err.Error(),
				Data:    nil,
			})

		}
	}

	return c.JSON(ResponseHTTP{
		Success: true,
		Message: "Success get book by ID.",
		Data:    *book,
	})
}

// RegisterBook registers a new book data
// @Summary 注册一本新的图书
// @Description 注册图书
// @Tags Book
// @Accept json
// @Produce json
// @Param book body models.BodyBook true "Register book"
// @Success 200 {object} ResponseHTTP{data=models.Book}
// @Failure 400 {object} ResponseHTTP{}
// @Router /v1/book [post]
func RegisterBook(c *fiber.Ctx) error {
	db := database.DBConn

	bodyBook := new(models.BodyBook)
	if err := c.BodyParser(&bodyBook); err != nil {
		return c.Status(http.StatusBadRequest).JSON(ResponseHTTP{
			Success: false,
			Message: err.Error(),
			Data:    nil,
		})
	}

	book := models.Book{
		Title:     bodyBook.Title,
		Publisher: bodyBook.Publisher,
		Author:    bodyBook.Author,
	}

	db.Create(&book)

	return c.JSON(ResponseHTTP{
		Success: true,
		Message: "Success register a book.",
		Data:    book,
	})
}

// DeleteBook function removes a book by ID
// @Summary 通过ID移除图书
// @Description 通过ID移除图书
// @Tags Book
// @Accept json
// @Produce json
// @Param id path int true "Book ID"
// @Success 200 {object} ResponseHTTP{}
// @Failure 404 {object} ResponseHTTP{}
// @Failure 503 {object} ResponseHTTP{}
// @Router /v1/book/{id} [delete]
func DeleteBook(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn

	book := new(models.Book)
	if err := db.First(&book, id).Error; err != nil {
		switch err.Error() {
		case "record not found":
			return c.Status(http.StatusNotFound).JSON(ResponseHTTP{
				Success: false,
				Message: fmt.Sprintf("Book with ID %v not found.", id),
				Data:    nil,
			})
		default:
			return c.Status(http.StatusServiceUnavailable).JSON(ResponseHTTP{
				Success: false,
				Message: err.Error(),
				Data:    nil,
			})

		}
	}

	db.Delete(&book)

	return c.JSON(ResponseHTTP{
		Success: true,
		Message: "Success delete book.",
		Data:    nil,
	})
}

// UpdateBook function removes a book by ID
// @Summary 通过ID更新图书
// @Description 通过ID更新图书
// @Tags Book
// @Accept json
// @Produce json
// @Param id path int true "Book ID"
// @Param book body models.BodyBook true "Register book"
// @Success 200 {object} ResponseHTTP{}
// @Failure 404 {object} ResponseHTTP{}
// @Failure 503 {object} ResponseHTTP{}
// @Router /v1/book/{id} [put]
func UpdateBook(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn

	book := new(models.Book)
	if err := db.First(&book, id).Error; err != nil {
		switch err.Error() {
		case "record not found":
			return c.Status(http.StatusNotFound).JSON(ResponseHTTP{
				Success: false,
				Message: fmt.Sprintf("Book with ID %v not found.", id),
				Data:    nil,
			})
		default:
			return c.Status(http.StatusServiceUnavailable).JSON(ResponseHTTP{
				Success: false,
				Message: err.Error(),
				Data:    nil,
			})

		}
	}

	bodyBook := new(models.BodyBook)
	if err := c.BodyParser(&bodyBook); err != nil {
		return c.Status(http.StatusBadRequest).JSON(ResponseHTTP{
			Success: false,
			Message: err.Error(),
			Data:    nil,
		})
	}

	book.Author = bodyBook.Author
	book.Publisher = bodyBook.Publisher
	book.Title = bodyBook.Title

	db.Save(&book)

	return c.JSON(ResponseHTTP{
		Success: true,
		Message: "Success update a book.",
		Data:    book,
	})
}
