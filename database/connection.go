package database

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	// DBConn is a pointer to gorm.DB
	DBConn   *gorm.DB
	user     = GetEnv("DB_USER", "kadycui")
	password = GetEnv("DB_PASSWORD", "123456")
	host     = GetEnv("DB_HOST", "127.0.0.1")
	db       = GetEnv("DB_NAME", "test")
	port     = GetEnv("DB_PORT", "5432")
)

// Connect creates a connection to database
func Connect() (err error) {
	port, err := strconv.Atoi(port)
	if err != nil {
		return err
	}
	dsn := fmt.Sprintf("user=%s password=%s host=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Shanghai", user, password, host, db, port)
	DBConn, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}

	sqlDB, err := DBConn.DB()

	if err != nil {
		return err
	}

	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(50)
	sqlDB.SetConnMaxLifetime(time.Hour)

	return nil
}

func GetEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}
